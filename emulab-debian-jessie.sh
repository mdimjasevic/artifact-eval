#!/bin/bash

# To the extent possible under law, Marko Dimjašević
# (https://dimjasevic.net/marko) has waived all copyright and related
# or neighboring rights to this script.

# The script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# This script assumes a clean Debian 8 64-bit installation


# Configure a language

source locale.sh


# Remove Debian non-free, set repos to Jessie
# The contrib section is needed for VirtualBox
sources_list="
deb http://ftp.us.debian.org/debian/ jessie main contrib
deb-src http://ftp.us.debian.org/debian/ jessie main contrib

deb http://security.debian.org/ jessie/updates main contrib
deb-src http://security.debian.org/ jessie/updates main contrib

# jessie-updates, previously known as 'volatile'
deb http://ftp.us.debian.org/debian/ jessie-updates main contrib
deb-src http://ftp.us.debian.org/debian/ jessie-updates main contrib"

src_file=/tmp/sources.list
dest_file=/etc/apt/sources.list

echo "$sources_list" > $src_file
sudo mv $dest_file $dest_file.old
sudo mv $src_file $dest_file

sudo apt-get update
