#!/bin/bash

# To the extent possible under law, Marko Dimjašević
# (https://dimjasevic.net/marko) has waived all copyright and related
# or neighboring rights to this script.

# The script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# This script assumes a Debian Jessie 64-bit installation

set -e

# Configure a language

source locale.sh

libvirt_dir=/mnt/storage/libvirt

# Install needed packages

sudo apt-get install --assume-yes \
    htop screen tree git build-essential xsltproc libxml2-utils qemu-kvm \
    python-gi python-ipaddr unar python-libvirt python-libxml2 \
    python-urlgrabber libvirt-daemon libvirt-daemon-system virt-manager \
    nfs-kernel-server


# Configure libvirt's authentication for remote access

remote_access_pkla="
[Allow $USER libvirt management permissions]
Identity=unix-user:$USER
Action=org.libvirt.unix.manage
ResultAny=yes
ResultInactive=yes
ResultActive=yes"

pkla_tmp_file=/tmp/50-emulab-libvirt-remote-access.pkla
polkit_dir=/etc/polkit-1/localauthority/50-local.d
sudo mkdir -p ${polkit_dir}
pkla_file=${polkit_dir}/50-emulab-libvirt-remote-access.pkla

echo "$remote_access_pkla" > $pkla_tmp_file
sudo mv $pkla_tmp_file $pkla_file
sudo chmod go-rwx $pkla_file
sudo chown root:root $pkla_file


# Enable remote access to libvirt, set its default url, and add your
# user name to the libvirt and kvm groups

libvirtd_conf=/etc/libvirt/libvirtd.conf
sudo sed -i '/unix_sock_group/c\unix_sock_group = \"libvirt\"' $libvirtd_conf
sudo sed -i '/unix_sock_rw_perms/c\unix_sock_rw_perms = \"0770\"' $libvirtd_conf

libvirt_conf=/etc/libvirt/libvirt.conf
sudo sed -i '/uri_default/c\uri_default = \"qemu:\/\/\/system\"' $libvirt_conf

sudo usermod -a -G libvirt $USER
sudo usermod -a -G kvm $USER


# Unload and then load kvm modules to reflect new user permissions in
# the kvm group

sudo rmmod kvm_intel
sudo rmmod kvm
sudo modprobe kvm
sudo modprobe kvm_intel

sudo chown root:kvm /dev/kvm

# Move libvirt-qemu VM config files and all other config files to a
# different location and restart libvirt

sudo /etc/init.d/libvirtd stop

new_libvirt_etc_dir=${libvirt_dir}/etc/libvirt
mkdir -p ${new_libvirt_etc_dir}

sudo mv --no-clobber /etc/libvirt/* ${new_libvirt_etc_dir}/
sudo rm -rf /etc/libvirt
sudo ln -s ${new_libvirt_etc_dir} /etc/

sudo /etc/init.d/libvirtd restart
sudo /etc/init.d/libvirt-guests restart


# Start and autostart the default libvirt network

set +e
sudo virsh net-start default
sudo virsh net-autostart default
set -e

# Change the default storage pool

storage_dir=${libvirt_dir}/images

set +e
sudo virsh pool-destroy default
sudo virsh pool-undefine default
set -e
sudo virsh pool-define-as --name default --type dir --target ${storage_dir}
sudo virsh pool-autostart default
sudo virsh pool-build default
sudo virsh pool-start default


# Restart the NFS server

sudo /etc/init.d/nfs-kernel-server restart
