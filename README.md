Tools you will find here are meant for setting up a virtual machine
environment that is to be managed remotely. Such an environment can be used
for [research artifact evaluation](http://www.artifact-eval.org/) and probably other things. The environment is
meant to be used in an [Emulab](http://emulab.net/) testbed, but it can be easily adopted for usage
in other scenarios.

# Emulab experiment

Set up an Emulab experiment with a single beefy machine. The machine should
hopefully have multiple processor cores and several dozens of gigabytes of
RAM memory. You can adopt this [experiment configuration](emulab.ns).

# Install Debian Jessie

Make sure to have at least Debian Jessie (that is Debian version 8). The
following will replace apt source files and get the latest list of packages:

    ./emulab-debian-jessie.sh

# Set up libvirt

The tools are based around [libvirt](https://en.wikipedia.org/wiki/Libvirt), a free software abstraction tool for
managing platform virtualization. Powered with the [Virtual Machine Manager](https://en.wikipedia.org/wiki/Virtual_Machine_Manager)
(also known as virt-manager), one can take any virtual appliance and run it
with free software only. In other words, there is no need to use proprietary
virtualization software such as VMware. Therefore, one can easily take a
VMware virtual appliance and convert it to a format that libvirt can manage
with no problems. All one needs to get is a virtual appliance in the VMX, ZIP,
or the [OVA/OVF](https://en.wikipedia.org/wiki/Open_Virtualization_Format) format and the virtual machine manager will be able to convert
it to the libvirt format.

With Debian Jessie in place, the following will install libvirt and
virt-manager and configure everything needed to remotely access virtual
appliances:

    source libvirt-setup.sh

# Set up Vagrant

If you want to use Vagrant through libvirt, Vagrant comes with a plugin that
makes it possible to use Vagrant with libvirt as the provider. There is
another plugin that converts Vagrant VirtualBox boxes into libvirt boxes. So
as long as someone provides you with a box that is for VirtualBox, you are
fine.

To install Vagrant and the plugins, run:

    ./vagrant-setup.sh

# Add Virtual Appliances

virt-manager makes it super easy to convert a virtual appliance to the libvirt
format and immediately start it. If the appliance is contained in a file named
`appliance.ova`, the following command will convert it and start it:

    sudo virt-convert appliance.ova --noautoconsole

## Vagrant appliances

In case someone provided you with a Vagrant VirtualBox box, you can convert it
to a libvirt box by using the `vagrant-mutate` plugin:

    vagrant mutate <box-name-or-file-or-url> libvirt

From there you initialize a Vagrant virtual appliance the usual way:

    vagrant init <box-name-or-file-or-url>

and then start it:

    vagrant up --provider=libvirt

# Creating a Virtual Machine With Vagrant

If paper authors have not provided a virtual appliance, you will need to
create a virtual machine (VM) yourself. It is a good idea to download a
Vagrant box first and then create a virtual machine with Vagrant based on the
box. At <http://vagrantcloud.com/> you can find a lot of boxes. For example, to
download a Debian 8 x64 VirtualBox box, you can run:

    vagrant box add deimosfr/debian-jessie

and then convert it to a libvirt box:

    vagrant mutate deimosfr/debian-jessie libvirt

Next, create a directory where the VM's files will be, e.g.:

    mkdir cava
    cd cava

Now you can create the VM based on the Debian box:

    vagrant init deimosfr/debian-jessie
    vagrant up --provider=libvirt

If the paper authors have provided you with a Bash script `install.sh` that
installs the tool and its dependencies, you can tell Vagrant to execute the
script within the VM. Do this by modifying the `Vagrantfile` that was created
with the `vagrant init` command above such that the `Vagrant.configure`
section has the following command somewhere:

    config.vm.provision "shell", path: "install.sh"

Similarly, there are other [provisioners](https://docs.vagrantup.com/v2/provisioning/index.html) that you can use with Vagrant, such as
Puppet, Chef, Salt, and others.

If all you need is a command line interface in the machine, you can use `ssh`
to connect to it:

    vagrant ssh
